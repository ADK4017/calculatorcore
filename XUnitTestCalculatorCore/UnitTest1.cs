using System;
using Xunit;
using CalculatorCore;

namespace XUnitTestCalculatorCore
{
    public class UnitTest1
    {
        [Fact]
        public void test_calculate_empty_imput()
        {
            //Given
            string empty_string = string.Empty;
            //When
            int result = Calculator.calculator(empty_string);
            int predicted_result = 0;
            //Then
            //Assert.Equal(result, predicted_result);
            Assert.Equal(result, predicted_result);
        }

        [Fact]
        public void test_calculate_one_number()
        {
            //Given
            string one_number = "4";
            //When
            int result = Calculator.calculator(one_number);
            int predicted_result = 4;
            //Then
            Assert.Equal(result, predicted_result);
        }

        [Fact]
        public void test_calculate_two_numbers()
        {
            //Given
            string two_numbers_string = "4,75";
            //When
            int result = Calculator.calculator(two_numbers_string);
            int predicted_result = 79;
            //Then
            Assert.Equal(result, predicted_result);
        }

        [Fact]
        public void test_calculate_three_numbers()
        {
            //Given
            string three_numbers_string = "3,45,7";
            //When
            int result = Calculator.calculator(three_numbers_string);
            int predicted_result = 55;
            //Then
            Assert.Equal(result, predicted_result);
        }

        [Fact]
        public void test_calculate_negative_number()
        {
            //Given
            string negative_number_string = "-3,45,7";
            //When
            //Then
            Assert.Throws<ArgumentException>(() => Calculator.calculator(negative_number_string));
        }

        [Fact]
        public void test_alculate_too_big_number()
        {
            //Given
            string too_big_numbers_string = "1001,45,7";
            //When
            int result = Calculator.calculator(too_big_numbers_string);
            int predicted_result = 52;
            //Then
            Assert.Equal(result, predicted_result);
        }

        [Fact]
        public void test_calculate_new_separator()
        {
            //Given
            string new_separator_string = "//#\n4#6#2";
            //When
            int result = Calculator.calculator(new_separator_string);
            int predicted_result = 12;
            //Then
            Assert.Equal(result, predicted_result);
        }

        [Fact]
        public void test_calculate_new_compound_separator()
        {
            //Given
            string new_compound_separator_string = "//[###]\n4###6###2";
            //When
            int result = Calculator.calculator(new_compound_separator_string);
            int predicted_result = 12;

            //Then
            Assert.Equal(result, predicted_result);
        }

        [Fact]
        public void test_calculate_many_compound_separators()
        {
            //Given
            string compound_separators_string = "//[###],[??]\n4###6??2";
            //When
            int result = Calculator.calculator(compound_separators_string);
            int predicted_result = 12;

            //Then
            Assert.Equal(result, predicted_result);
        }

        [Fact]
        public void test_calculate_newline_separators()
        {
            //Given
            string newline_separators_string = "5\n6\n7";
            //When
            int result = Calculator.calculator(newline_separators_string);
            int predicted_result = 18;

            //Then
            Assert.Equal(result, predicted_result);
        }
    }
}
