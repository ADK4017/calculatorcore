﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CalculatorCore
{
    public static class Calculator
    {
        public static int calculator(string number_string)
        {
            if (number_string == string.Empty)
                return 0;

            Change_Separator_To_Coma(ref number_string);

            int index_of_line_separator = number_string.IndexOf('\n');
            if (index_of_line_separator != -1)
            {
                number_string = number_string.Remove(0, index_of_line_separator + 1);
            }

            string[] numbers = number_string.Split(',');

            int sum = 0;

            for (int i = 0; i < numbers.Length; i++)
            {
                int number;
                if (int.TryParse(numbers[i], out number))
                {
                    if (number < 0)
                    {
                        throw (new ArgumentException("negative numbers are not supported"));
                    }
                    else if (number > 1000)
                    {
                        continue;
                    }
                    else
                    {
                        sum += number;
                    }
                }
                else
                {
                    continue;
                }
            }

            return sum;
        }

        private static void Change_Separator_To_Coma(ref string number_string)
        {
            string[] numbers = number_string.Split('\n');
            if (numbers.Length == 1)
                return;

            string separator_string = numbers[0];

            if (separator_string.StartsWith("//") == true)
            {
                separator_string = separator_string.Remove(0, 2);
                if (separator_string.Length == 1)
                {
                    number_string = number_string.Replace(separator_string, ",");
                    return;
                }
                string[] separators = separator_string.Split(',');
                for (int i = 0; i < separators.Length; i++)
                {
                    string pattern = separators[i].Remove(0, 1);
                    pattern = pattern.Remove(pattern.Length - 1, 1);
                    number_string = number_string.Replace(pattern, ",");
                }
            }
            else
            {
                number_string = number_string.Replace("\n", ",");
            }

        }
    }
}
